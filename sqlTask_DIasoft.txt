-- 1. Найти всех пользователей, которые зарегистрировались, но не подтвердили почту.
select * from tUser
where tUser.userid not in (select userid from tAudit where tAudit.actiontype=2);


-- 2. Найти всех пользователей у которых просрочился токен доступа, текущую дату время из функции GetDate()
select *,CURRENT_TIMESTAMP from tUser
where tUser.userid in (select userid from tAccessToken where CURRENT_TIMESTAMP>tAccessToken.expiredate);

-- 3. Найти всех пользователей которые зарегистрировались, но не разу не заходили в систему.
select * from tUser
where tUser.userid not in (select userid from tAudit where tAudit.actiontype=3); -- если брать в расчет тех, кто либо зарегистрировались, либо подтвердили почту
and tUser.userid in (select userid from tAudit where tAudit.actiontype=2); -- дополнение, если учитывать только тех, кто подтвердили почту

--4. Найти первых четырех пользователей, которые чаще всего неверно ввродят пароль.
select tUser.userid,name,pass,mail from tUser
inner join tAudit on tUser.userid=tAudit.userid
where tAudit.actiontype=5
group by tUser.userid
order by count(tAudit.auditid) desc
limit 4;