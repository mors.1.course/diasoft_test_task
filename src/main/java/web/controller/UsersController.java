package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import web.model.User;
import web.service.ObjService;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {

	ObjService service;

	@Autowired
	public UsersController(ObjService service) {
		this.service = service;
	}

	@GetMapping()
	public String printUsers(ModelMap model) {
		List<User> users=service.getUsersList();
		model.addAttribute("users", users);
		return "users";
	}

	@GetMapping(value = "/new")
	public String createNewUser(@ModelAttribute("user") User user,ModelMap model) {
//		model.addAttribute("user",new User());
		return "new";
	}

	@PostMapping()
	public String create(@ModelAttribute("user") User user) {
		service.add(user);
		return "redirect:/users";
	}

	@GetMapping(value = "/{id}")
	public String printUser(@PathVariable("id")long id,ModelMap model) {
		model.addAttribute("user",service.getUser(id));
		return "user";
	}

	@GetMapping(value = "/{id}/edit")
	public String editUser(@PathVariable("id") long id,ModelMap model) {
		model.addAttribute("user",service.getUser(id));
		return "edit";
	}

	@PatchMapping("/{id}")
	public String edit(@ModelAttribute("user") User user,@PathVariable("id") long id){
		service.update(user);
	return "redirect:/users/"+id;
	}
	@DeleteMapping("/{id}")
	public String delete(@PathVariable("id") long id){
		service.delete(id);
		return "redirect:/users";
	}
}