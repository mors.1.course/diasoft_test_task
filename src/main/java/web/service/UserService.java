package web.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import web.dao.Dao;
import web.model.User;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service("serviceImpl")
public class UserService implements ObjService {
    List<User> users = new ArrayList<>();
    Logger log = LoggerFactory.getLogger(UserService.class);
    private static int counter;

    Dao dao;

    @Autowired
    public UserService(Dao dao) {
        this.dao = dao;
    }

//    @PostConstruct
    private void addDefaultUsers(){

        users.add(new User("Nick", "Kad", (byte) 24,"nik@mail.com"));
        users.add(new User("Ulya", "Kad", (byte) 26,"ulya@mail.com"));
        users.add(new User("Ivan", "Ivanov", (byte) 29,"ivan@mail.com"));
        users.add(new User("Sergey", "Bodrov", (byte) 30,"sergey@mail.com"));
        for (User user : users) {
            dao.add(user);
        }
    }

    @Override
    @Transactional
    public List<User> getUsersList() {
        if(users.size()==0) {
            addDefaultUsers();
        }
       return dao.listUsers();
//        return users.stream().limit(Integer.parseInt(count)).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void add(User user) {
        int c= counter;
        log.debug("Transaction-{} is going...",c);
        dao.add(user);
        log.debug("Transaction-{} is done: {} has been saved as {} at {}",c,user,user.getClass().getSimpleName(), LocalTime.now());
        counter++;
    }

    @Override
    @Transactional
    public void delete(long id) {
        int c= counter;
        log.debug("Transaction-{} is going...",c);
        User deletedUser = getUser(id);
        dao.delete(id);
        log.debug("Transaction-{} is done: {} has been deleted at {}",c,deletedUser, LocalTime.now());
        counter++;
    }

    @Override
    @Transactional
    public void update(User user) {
        int c= counter;
        log.debug("Transaction-{} is going...",c);
        dao.update(user);
        log.debug("Transaction-{} is done: {} has been updated at {}",c,user,LocalTime.now());
        counter++;
    }

    @Override
    @Transactional
    public User getUser(long id) {
        return dao.getUser(id);
    }
}
